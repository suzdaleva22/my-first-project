/*
1) Створіть обʼєкт triangle з наступними властивостями: aSide, bSide, cSide.

Створіть метод setValues(), який питає у користувача значення і додає їх в ключі aSide, bSide, cSide.

Створіть в обʼєкті triangle метод для підрахунку периметра трикутника;
Створіть в обʼекті triangle метод, який перевіряє чи є цей трикутник рівностороннім;
Виведіть інформацію про створений трикутник (включаючи результати роботи усіх методів) в консоль.

let triangle = {
  aSide: 0,
  bSide: 0,
  cSide: 0,
  setValues: function() { <- это метод объекта triangle
    // your code should be here
  },
  getPerimeter: function() { <- это метод объекта triangle
    // your code should be here
  },
  isEqualSides: function() { <- это метод объекта triangle
    // your code should be here
  },
}
В консоли очікую побачити:

triangle.setValues(); // for examle 3 3 3
console.log(triangle.getPerimeter()); // 9
console.log(triangle.isEqualSides()); // true
*/


let triangle = {
  aSide: 0,
  bSide: 0,
  cSide: 0,
  setValues: function() { 
    this.aSide = +prompt('Enter the value of side a');
    this.bSide = +prompt('Enter the value of side b');
    this.cSide = +prompt('Enter the value of side c');
  },
  getPerimeter: function() { 
    return this.aSide + this.bSide + this.cSide;
  },
  isEqualSides: function() {
    return this.aSide === this.bSide && this.aSide === this.cSide;
  }
};

triangle.setValues();
console.log(triangle.getPerimeter());
console.log(triangle.isEqualSides());
console.log(triangle);

/*
2) Створіть обʼєкт calculator з методами:

read() визиває prompt() для заповнення двох значень і зберігає їх як властивості обʼєкту x, y;
sum() повертає суму цих двох значень;
multi() повертає добуток цих двох значень;
diff() повертає різницю цих двох значень;
div() повертає ділення цих двох значень;
В консоли очікую побачити:

calculator.read(); // for example 5 2
console.log(calculator.sum()); // 7
console.log(calculator.diff()); // 3


*/

let calculator = {
  firstNumber: 0,
  secondNumber: 0,
  getNumbers: function() { 
    this.firstNumber = +prompt('Enter the first number');
    this.secondNumber = +prompt('Enter the second number');
  },
  getSum: function() { 
    return this.firstNumber + this.secondNumber;
  },
  getProduct: function() {
    return this.firstNumber * this.secondNumber;
  },
  getDifference: function() { 
    return this.firstNumber - this.secondNumber;
  },
  getQuotient: function() { 
    return this.firstNumber / this.secondNumber;
  },
};

calculator.getNumbers(); // for example 5 2
console.log(calculator.getSum()); // 7
console.log(calculator.getDifference()); // 3


/*
3) Даний обʼєкт country і функція format():

var country = {
    name: 'Ukraine',
    language: 'ukrainian',
    capital: {
        name: 'Kyiv',
        population: 2907817,
        area: 847.66
    }
 };

function format(start, end) {
    console.log(start + this.name + end);
 }
Допишіть код так, щоб в консолі зʼявились рядки, які вказані в коментарях.

format.call(/ Ваш код /); // Ukraine
format.apply(/ Ваш код /); // [Ukraine]
format.call(/ Ваш код /); // Kyiv
format.apply(/ Ваш код /); // Kyiv
format.apply(/ Ваш код /); // undefined
*/

var country = {
  name: 'Ukraine',
  language: 'ukrainian',
  capital: {
      name: 'Kyiv',
      population: 2907817,
      area: 847.66
  }
};

function format(start, end) {
    console.log(start + this.name + end) ;
  };

format.call(country, '', '');
format.apply(country, ['[', ']']); // [Ukraine]
format.call(country.capital, '', ''); // Kyiv
format.apply(country.capital, ['', '']); // Kyiv

//////////////////////////////
format.apply( {}, ['', ''] );  // // undefined---  Ура! спасибо! оказалось так просто)
//???????????????//////////////////////////////

/*
Що поверне даний код на екран і чому?
*/
var text = 'outside';
function logIt(){
    console.log(text);
    var text = 'inside';
};
logIt();

///в консоли отобразиться undefined, Так как в функции console задан аргумент test, JS  сначала ищет переменнную test в  локальной области видимости. Переменнная test объявляется ниже её вызова. Так как она  является var, она инициализируется со значением undefined (всплытие переменной var).