//-----ПЕРЕМЕННЫЕ. ТИПЫ ДАННЫХ-----

// 1) Напишите программу, которая спрашивает у пользователя сколько ему лет (команда prompt()) и выводит его возраст на экран (команда alert())

const userAge = prompt('How old are you?'); 
alert(userAge);

//2) Напишите программу, которая спрашивает у пользователя как его зовут, в какой стране и в каком городе он живет (команда prompt()) и выведите результат в консоль в следующем виде (команда console.log())

const username = prompt('What is your name?'); 
const city = prompt('What city do you live in?'); 
const country = prompt('What country do you live in?'); 
console.log(`Hello, your name is ${username}. You live in ${city}, ${country}`);


//3) Напишите программу, которая выводит в консоль (команда console.log()) тип 3х разных переменных. 

let phone = 156789;
let schoolName = 'IT school Hillel';
let stage = false;

console.log(`value: ${phone}; type: ${typeof(phone)}`);
console.log(`value: '${schoolName}'; type: ${typeof(schoolName)}`);
console.log(`value: ${stage}; type: ${typeof(stage)}`);
