/* Initial song list */
let songs = [{
    name: 'Jingle Bells',
    isLiked: false,
  }, {
    name: 'We Wish You a Merry Christmas',
    isLiked: true,
  }];


  const listOfSongs = document.querySelector('ul.songs');
  
  function createButton (item, className, buttonText, clickEvent) {
    const button = document.createElement('button');
    button.addEventListener('click', clickEvent);
    button.setAttribute('class', className);
    button.textContent = buttonText;
    item.append(button);  
  };
  
  function createHearth (item) {
    const imageLike = document.createElement('img');
    imageLike.setAttribute('src', './images/like.svg');
    imageLike.setAttribute('class', 'like-icon');
    imageLike.style.width = '3%';
    item.prepend(imageLike);
  };

  const buttonAdd = document.querySelector('.add');
  buttonAdd.addEventListener('click', function() {
    const userSong = document.querySelector('.input-box');
    let newSong = {
      name: userSong.value,
      isLiked: false, 
      };
    addItem(newSong, newSong.name, newSong.isLiked); 
    songs.push(newSong);
    userSong.value = '';
    console.log(songs);
    countSongs ();
  });

  function addItem (arr, name, isLiked) {
    const song = document.createElement('li');
    song.textContent = name;
    song.setAttribute('class', isLiked);
    listOfSongs.append(song);
    if (isLiked) {
      createButton (song, 'button like', 'Unlike', addLike);
      createHearth (song);
    } else {
      createButton (song, 'button like', 'Like', addLike);
    }
    createButton (song, 'button delete', 'Delete', deleteItem);
  };

  function addLike (event) {
    const songElements = document.querySelectorAll('li');
    const buttonLike = document.querySelectorAll('.like');
    for (let i = 0; i < buttonLike.length; i++) {
      if (buttonLike[i] == event.target){
        if ( buttonLike[i].textContent === 'Unlike' ) {
          buttonLike[i].textContent = 'Like';
          songElements[i].removeChild(songElements[i].firstElementChild);
          songs[i].isLiked = false;
        } else {
          buttonLike[i].textContent = 'Unlike';
          createHearth (songElements[i]);
          songs[i].isLiked = true;
        };
      };
    };
  };

  function countSongs () {
    let count = document.querySelector('.count');
    count.textContent = songs.length;
  };

  function deleteItem (event) {
    const buttonDelete = document.querySelectorAll('.delete');
    const songElements = document.querySelectorAll('li');
    for (let i = 0; i < buttonDelete.length; i++) {
      if (buttonDelete[i] == event.target) {
        songElements[i].remove();
        songs.splice(i, 1) 
        console.log(songs);
        console.log (songElements)
        countSongs (songElements); 
        return songElements;
      }; 
    };
  };
 
  window.addEventListener('load', function() {
    for (let item of songs) {
      addItem(songs, item.name, item.isLiked)
    };
    countSongs ();
  });