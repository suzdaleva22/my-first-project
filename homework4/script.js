//-----Цикли-домашка 4----
// 1) Напишіть програму, яка питає у користувача його імʼя і виводить в консолі текстове привітання.

// Happy birthday to you
// Happy birthday to you
// Happy birthday, dear <name>
// Happy birthday to you

{
    const userName = prompt(`What is your name?`);
    let song = ''; 

    for (i = 1; i <= 4; i++) {
        if(i == 3) {
            song+=`Happy birthday, dear ${userName}\n`;
        } else {
            song += `Happy birthday to you\n`;
        }
    };
    console.log(song); 
}

//2) 2) Сформуйте строку '.#.#.#.#.#.#.#' за допомогою циклу for, де необхідну кількість повторів символів '.#' задає користувач через команду prompt().

{
    const code = +prompt(`Set quantity #`);
    let str = ''; 
    const userSymbol = `.#`;

    for (i = 1; i <= code; i++) {
        str += userSymbol;
    };
    console.log(str); 
}


//3) 3) Напишіть програму, яка питає у користувача число і сумує всі непарні числа до цього числа.

// Якщо користувач ввів не число або відʼємне число чи 0, визивати команду prompt() з текстом "Invalid. You should enter a number" до тих пір поки вірний формат даних не буде введений користувачем.

// Результат відобразіть в консолі.

{
    let number = +prompt('Enter a number');
    let sum = 0;


    for (i = 1; isNaN(number) || !Number.isInteger(number) || number <= 0; i++) {
        number = +prompt('Invalid. You should enter a number');  
    };
    for (i = 1; i < number; i++) {
        if (i % 2)
        sum += i;
    };
    console.log(sum); 
}

//4) Напишіть нескінченний цикл, який закінчується командою break, коли Math.random() > 0.7. Виведіть в консоль число, на якому переривається цикл та відобразіть в консолі кількість ітерацій циклу.

// Loop was finished because of: <number>
// Number of attempts: <number>

{
    let attempt = 0;

    for (;;) {
        let randomNumber = Math.random();
        attempt++;
        if (randomNumber > 0.7) {
            console.log(`Set Loop was finished because of: ${randomNumber}\nNumber of attempts: ${attempt}`);
            break; 
        }; 
    };
}
//5) Напишіть цикл від 1 до 50, в якому будуть виводитися числа по черзі від 1 до 50, при цьому:

// Якщо число ділиться на 3 без залишку, то виведіть не це число, а слово 'Fizz';
// Якщо число ділиться на 5 без залишку, то виведіть не це число, а слово 'Buzz';
// Якщо число ділиться і на 3 і на 5 одночасно, то виведіть не це число, а слово 'FizzBuzz';

{
    let list = ''; 

    for (i = 1; i <= 50; i++) {
        if (i % 3 == 0 && i % 5 == 0){
            list+=`FizzBuzz\n`; 
        } else if (i % 3 == 0) {
            list+=`Fizz\n`;
        } else if (i % 5 == 0){
            list+=`Buzz\n`; 
        } else {
            list+=`${i}\n`;   
        };
    };
    console.log(list); 
}

//6)) Напишіть програму, яка знайде всі роки, коли 1 січня випадає на неділю у період між 2015 та 2050 роками включно (зверніть увагу, що 1 січня в лапках).

//"1st of January" is being a Sunday in <year>

let year;

for (year = 2015; year <= 2050; year++) {
    let date = new Date(year, 0, 1);
     if (date.getDay() == 0) {
        console.log(`"1st of January" is being a Sunday in ${year}`); 
   };
};
