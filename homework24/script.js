/*
Задача:

Напишіть програму, яка питає у користувача дату в форматі YYYY-MM-DD і виводить в консоль всі дати від заданої дати до сьогоднішньої.

Обовʼязкові критерії:

дата повинна бути валідна і в строго заданому форматі YYYY-MM-DD (краще за все перевіряється за допомогою регулярних виразів);
формат виводу даних довільний, але обовʼязково відображати день;
*/

const userDate = prompt('Write the date in the format YYYY-MM-DD');
const initialDate = new Date(userDate);
const currentDate = new Date();

function getDates() {
  let datePattern = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
  if (initialDate == null || initialDate == "" || !datePattern.test(userDate) || initialDate > currentDate) {
    console.log("Invalid date");
    return false;
  } else {
    const date = {
      from: initialDate,
      to: currentDate,
      [Symbol.iterator]() {
        return {
          dateFrom: this.from,
          currentDate: this.to,
          dayMilliseconds: 24 * 60 * 60 * 1000,
          next() {
            if (this.dateFrom < this.currentDate) {
              return {
                done: false,
                value: this.dateFrom = new Date(Date.parse(this.dateFrom) + this.dayMilliseconds)
              };
            }
            return {
              done: true,
              value: this.dateFrom = new Date(Date.parse(this.dateFrom) + this.dayMilliseconds)
            };
          },
        };
      },
    };
    for (let value of date) {
      console.warn('Date: ', formatDate(value));
    }
  }
}

getDates();

function formatDate(date) {
  month = '' + (date.getMonth() + 1),
  day = '' + date.getDate(),
  year = date.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;
  return [day, month, year].join('/');
}