"use strict";
const url = `http://api.weatherstack.com/current`;
const accessKey = localStorage.getItem("access_key");
const cities = ["Lviv", "Kiev", "Kharkiv", "Donezk", "Dnipro"];

async function getData(city) {
  const data = await fetch(`${url}?access_key=${accessKey}&query=${city}`);
  return data.json();
}

async function start(city) {
  try {
    const receivedData = await getData(city);
    clearBox("weather");
    createCard(receivedData);
  } catch (err) {
    console.error(err);
  }
}

function clearBox(idDiv) {
  document.getElementById(idDiv).outerHTML = "";
}

window.addEventListener("load", function () {
  showLoader();
  for (let item of cities) {
    const listOfCities = document.querySelector("ul.animate");
    const citySelect = document.createElement("li");
    citySelect.textContent = item;
    citySelect.classList.add("animate");
    listOfCities.append(citySelect);
  }
  setDefaultCity();
  start(cities[0]);
});

function selectCity() {
  const listOfCities = document.querySelector("ul.animate");
  listOfCities.addEventListener("click", (event) => {
    let addedCities = document.querySelectorAll("li.animate");
    for (let item of addedCities) {
      item.classList.remove("selected");
    }
    const target = event.target.textContent;
    event.target.classList.add("selected");
    start(target);
  });
}

selectCity();

function setDefaultCity() {
  const defaultCity = document.querySelector("li.animate");
  defaultCity.classList.add("selected");
}

const container = document.querySelector(".wrapper");

function createCard(data) {
  const card = document.createElement("div");
  card.classList.add("weather");
  card.setAttribute("id", "weather");

  const cardHeader = document.createElement("div");
  cardHeader.classList.add("weather__header");

  const cardMain = document.createElement("div");
  cardMain.classList.add("weather__main");

  const cardTitle = document.createElement("div");
  cardTitle.classList.add("weather__city");
  cardTitle.textContent = data.location.name;

  const weatherStatus = document.createElement("div");
  weatherStatus.classList.add("weather__status");
  weatherStatus.textContent = data.current.weather_descriptions;

  const weatherIcon = document.createElement("div");
  weatherIcon.classList.add("card-weather__icon");
  const icon = document.createElement("img");
  icon.src = data.current.weather_icons;
  icon.alt = "image";
  weatherIcon.append(icon);
  card.append(weatherIcon);

  const weatherTemp = document.createElement("div");
  weatherTemp.classList.add("weather__temp");
  weatherTemp.textContent = data.current.temperature;

  const weatherFeelsLike = document.createElement("div");
  weatherFeelsLike.classList.add("weather__feels");
  weatherFeelsLike.textContent = `Feels like: ${data.current.feelslike}`;

  card.append(cardHeader);
  card.append(weatherTemp);
  card.append(weatherFeelsLike);

  cardHeader.append(cardMain);
  cardHeader.append(weatherIcon);

  cardMain.append(cardTitle);
  cardMain.append(weatherStatus);

  container.append(card);
}

function showLoader() {
  const card = document.createElement("div");
  card.classList.add("weather");
  card.setAttribute("id", "weather");

  const cardHeader = document.createElement("div");
  cardHeader.classList.add("weather__loading");

  const iconLoader = document.createElement("img");
  iconLoader.src = "img/loading.gif";
  iconLoader.alt = "Loading...";

  card.append(cardHeader);
  cardHeader.append(iconLoader);

  container.append(card);
}
