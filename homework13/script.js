//'use strict';

/*
Создайте объект coffeeMachine со свойством message: ‘Your coffee is ready!’ и методом start(), при вызове которого через 3 секунды в консоль выведется сообщение, записанное в свойстве message.

Начальный код:

let coffeeMachine = {
   message: 'Your coffee is ready!',
   start: function() {
     // write your code here
   },
}
Результат:

coffeeMachine.start(); // 'Your coffee is ready!'


Создайте объект teaPlease со свойством message: 'Wanna some tea instead of coffee?'. Обновите методу start() контекст так, чтобы он выводил сообщение с нового объекта.

coffeeMachine.start(); // 'Wanna some tea instead of coffee?'


*/

let coffeeMachine = {
  message: 'Your coffee is ready!',
  start: function () {
    setTimeout(
      function () {
        console.log(this.message);
      }.bind(this),
      3000
    );
  },
};

coffeeMachine.start();

let teaPlease = {
  message: 'Wanna some tea instead of coffee?',
};

coffeeMachine.start.bind(teaPlease)();

/*
2) Напишите функцию concatStr(), которая соединяет две строки, разделенные каким-то символом: разделитель и строки передаются в параметрах функции. Используя привязку аргументов с помощью bind, создайте новую функцию hello(), которая которая выводит приветствие тому, кто передан в ее параметре:

let checkConcat = concatStr('Hello', ' ', 'Matt'); // 'Hello Matt'
...
let finalResult = hello('Matt') // 'Hello Matt'
let finalResult = hello('John') // 'Hello John'

 */

let checkConcat = concatStr('Hello', ' ', 'Matt'); // 'Hello Matt'

function concatStr(text, separator, userName) {
  let string = text + separator + userName;
  console.log(string);
}

const hello = concatStr.bind(undefined, 'Hello', ' ');

let finalResult = hello('John'); // 'Hello John'

/*
3) Напишите функцию showNumbers(), которая последовательно выводит в консоль числа в заданном диапазоне, с заданным интервалом (все данные должны передаваться как параметры функции).

tips: для реализации используйте функцию setInterval()

showNumbers(5, 10, 500); // 5 6 7 8 9 10
*/

function showNumbers(startNumber, finishNumber, timeInterval) {
  let i = startNumber;
  let timer = setInterval(function () {
    console.log(i);
    if (i == finishNumber) {
      clearInterval(timer);
    } else i++;
  }, timeInterval);
};

showNumbers(5, 10, 500); // 5 6 7 8 9 10

/*
) Какой результат выполнения будет у данного кода? Объяснить почему.

function addBase(base) {
  return function (num) {
    return base + num;
  };
}
let addOne = addBase(1);
alert(addOne(5) + addOne(3));
*/

function addBase(base) {
  return function (num) {
    return base + num;
  };
}
let addOne = addBase(1);
alert(addOne(5) + addOne(3));


/* Результат = 10. 
Функция возвращает функцию, в которой суммируются их аргументы 
стр 104 - Новой переменной-функции addOne присваеваем функцию addBase с аргументом base = 1
стр 105 вызываем функцию addOne с аргументами: 1) num = 5; 2) num = 3;
Суммируются результаты дважды вызванной функции (1+5) + (1+3)

*/