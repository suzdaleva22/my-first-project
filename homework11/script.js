/*
1) Допрацюйте масив employee, з яким ви вже працювали раніше.
*/

//1.1) Додайте у масив два нові обʼєкти (ключі повинні бути такі ж, а значення придумайте самі).
{
  empolyee.push({
    id: 12,
    name: 'Yulia',
    surname: 'Suzdalyeva',
    salary: 3030,      
    workExperience: 1,
    isPrivileges: false,
    gender: 'female',
    },
    {
    id: 13,
    name: 'Alex',
    surname: 'White',
    salary: 1010,
    workExperience: 30,
    isPrivileges: false,
    gender: 'male',
    });

  console.log(empolyee);

//1.2) Виведіть в консоль масив жінок-співробітниць з досвідом роботи менше ніж 10 місяців.

  const womenWithLittleWorkExperience = empolyee.filter(function (item) {
    return item.workExperience < 10 && item.gender === 'female';
  })
  
  console.log(womenWithLittleWorkExperience); 

//1.3) Виведіть в консоль обʼєкт співробітника, у якого id=4.

const employeeId4 = empolyee.find(function (item) {
  return item.id === 4;
});

console.log(employeeId4); 

//1.4) Виведіть в консоль масив прізвищ співробітників.

  let employeeSurnames = empolyee.map (function (item) {
    return item.surname;
  })
  console.log(employeeSurnames);
}

/*
2) Створіть масив frameworks зі значеннями: 'AngularJS', 'jQuery'
*/

let frameworks = ['AngularJS', 'jQuery'];

//a. Додайте на початок масиву значення 'Backbone.js'
frameworks.unshift('Backbone.js');

//b. Додайте до кінця масиву значення 'ReactJS' і 'Vue.js'
frameworks.push('ReactJS', 'Vue.js');

//с. Додайте в масив значення 'CommonJS' другим елементом масиву
frameworks.splice(1, 0, 'CommonJS');

//d. Знайдіть та видаліть із масиву значення 'jQuery' (потрібно знайти індекс елемента масиву) і виведіть його в консоль зі словами “Це тут зайве”

let index = frameworks.indexOf('jQuery');
const removed = frameworks.splice(index, 1);
console.log(`Це тут зайве - ${removed}`);

/*
3) Створіть функцію removeNegativeElements, яка видаляє з вхідного масиву всі негативні числа.

let result = removeNegativeElements([-9, 2, 3, 0, -28, 'value']); // [2, 3, 0, 'value'];
let result = removeNegativeElements([-9, -21, -12]; // []
let result = removeNegativeElements(['-102', 102]); // ['-102', 102]
let result = removeNegativeElements([NaN, 45, -5, null]); // [NaN, 45, null]

*/
{
  function removeNegativeElements(array) {
    return array.filter(function(item){
      return typeof(item) === 'string' || ( typeof(item) === 'number' && item > 0);
    });
  };

  let result1 = removeNegativeElements([-9, 2, 3, 0, -28, 'value']); // [2, 3, 0, 'value'];
  let result2 = removeNegativeElements([-9, -21, -12]); // []
  let result3 = removeNegativeElements(['-102', 102]); // ['-102', 102]
  let result4 = removeNegativeElements([NaN, 45, -5, null]); // [NaN, 45, null]
  
  console.log(result1);
  console.log(result2);
  console.log(result3);
  console.log(result4);
}
 /*
4) Створіть функцію getStringElements, яка повертає вхідний масив лише зі строковими значеннями.

let arr = [1 , true , 42 , "red" , 64 , "green" , "web" , new Date() , -898 , false]
let result = getStringElements(arr); // ["web", "green", "red" ]
 */
{
  function getStringElements(array) {
    return array.reverse().filter(function(item){
      return typeof(item) === 'string';
      });
    }; 

  let arr = [1 , true , 42 , 'red' , 64 , 'green' , 'web' , new Date() , -898 , false];
  let result = getStringElements(arr);

  console.log (result);
}

/*
5) Напишите функцию flatArray, которая превращает массив в массиве в единый массив.

tips: вам поможет в этом метод reduce() c начальным значением [] и spread оператор.

Напишіть функцію flatArray, яка перетворює масив масивів на єдиний масив.

let array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
let result = flatArray(arr); // [1, 2, 3, 4, 5, 6, 7, 8, 9] 


*/
{ ////////// vot tak rabotaet
let array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];

const flat = array.reduce((prev, item) => {
  return prev = [...prev, ...item];
}, []);

let result = flat;
console.log(result);
}

{/////////// a tak ne ho4et, ne mogu ponjat po4emu(( /// undefined
let array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
let result = flatArray(array); 

function flatArray(arr) {
  arr.reduce((prev, item) => {
    return prev = [...prev, ...item];
  }, []);
};
console.log(result);
}

/*
6) Створіть функцію myMap, яка приймає як аргумент масив та функцію, як цей масив має бути модифікований.

let arr = [1, 2, 3, 4];
function increaseElement (element) {
  return element * 2;
}
let result = myMap(arr, increaseElement) // [2, 4, 6, 8];

*/
{
  function myMap (array, func) {
    const newArray = array.map(func);
    return newArray;
  };

  function increaseElement (element) {
    return element * 2;
  };

  let arr = [1, 2, 3, 4];

  let result = myMap(arr, increaseElement) // [2, 4, 6, 8];
  console.log(result);
}
