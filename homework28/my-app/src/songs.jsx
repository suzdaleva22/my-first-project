import { Fragment, useState } from "react";
import { songs as songsData } from "./data/songs";
import Song from "./song";

export default function Songs() {
  const [songs, setSongs] = useState(songsData);
  if (!songs.length) {
    return <p>No songs in the playlist</p>;
  }
  return songs.map((song) => <Song song={song} key={song.id} />);
}
