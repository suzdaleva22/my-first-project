/*
1) Напишіть функцію fillArray, яка створює масив і заповнює його заданим значенням.

let array = fillArray(3, 'qwerty');
console.log(array); // ['qwerty', 'qwerty', 'qwerty']
де 3 - це довжина масиву, а 'qwerty' значення кожного елементу масива
*/

{
  let array = fillArray(3, 'qwerty');
  console.log(array); 

  function fillArray(lenght, aggregate) {
    let arr = new Array (lenght);
    arr.fill (aggregate);
    return arr;
  }
}

/*
2) Напишіть функцію reverseArray, яка перевертає значення масиву задом наперед.

let array = ['My', 'life', '-', 'my', 'rules'];
let result = reverseArray(array);
сonsole.log(result); // ['rules', 'my', '-', 'life', 'My'];

*/
{
  let array = ['My', 'life', '-', 'my', 'rules'];
  let result = reverseArray(array);
  console.log(result);
  
  function reverseArray(arr) {
    arr.reverse();
    return arr;
  };
}

/*
3) Напишіть функцію filterArray, яка очищує масив від небажаних значень (false, undefined, '', 0, null, NaN).

let array = [0, 1, 2, null, undefined, 'qwerty', false, NaN];
let result = filterArray(array);
console.log(result); // [1,2, 'qwerty'];
*/

{
  let array = [0, 1, 2, null, undefined, 'qwerty', false, NaN];
  let result = filterArray(array);
  console.log(result); 

  function filterArray (arr) {
    return arr.filter (function filterType (item) {
      if (typeof(item) == 'number' || typeof(item) == 'string') {
        return item;
     }
    });
  } 
}

/*
4) Напишіть функцію spliceFour, яка видаляє 4й елемент масиву і замінює його строкою 'JavaScript'

let array = [1, 2, 3, 4, 5];
let result = spliceFour(array);
console.log(result); // [1, 2, 3, 'JavaScript', 5];

*/


{
  let array = [1, 2, 3, 4, 5];
  let result = spliceFour(array);
  console.log(array);

  function spliceFour (arr) {
    let result = arr.splice(3, 1, 'JavaScript');
  }  
}

/*
5) Напишіть функцію joinArray, яка перетворює масив в строку, з'єднуючи елементи заданим символом.

let array = [1, 2, 3, 4, 5];
let result = joinArray(array, '%');
console.log(result); // 1%2%3%4%5
*/

{
  let array = [1, 2, 3, 4, 5];
  let result = joinArray(array, '%');
  console.log(result);

  function joinArray(arr, connector) {
    let string = arr.join(connector);
    return string;
  }
}


/*
6) Напишіть функцію joinStr, яка повертаэ строку, яка утворена з конкатинацїї усіх строк, які передані в якості аргументів функції через кому.

let string1 = joinStr(0);
console.log(string1); // ''
let string2 = joinStr(1,'hello',3, 'world');
console.log(string2); // 'hello,world'
let string3 = joinStr('g','o', 0, '0', null, 'd', {});
console.log(string3); // 'g,o,0,d'
*/
{
  function joinStr () {
    let str = [...arguments];
    let result = str.filter(filterType).join(',');

    return result;
  }

  function filterType (item) {
    if (typeof(item) == 'string') {
      return item;
    }
  }
  
  let string1 = joinStr(0);
  console.log(string1); 
  let string2 = joinStr(1,'hello',3, 'world');
  console.log(string2); 
  let string3 = joinStr('g','o', 0, '0', null, 'd', {});
  console.log(string3);
}

/*
7) Напишіть функцію advancedFillArray, яка створює масив і заповлює його випадковими значеннями чисел з заданого діапазону. (Логіку створення випадкового числа оберніть в функцію setRandomValue(min, max)).

let array  = advancedFillArray(5, 1, 15);
console.log(array); // [2, 4, 6, 13, 10];
де 5 - це довжина масива, 1 - це мінімальне значення, 15 - це максимальне значення.

tips: тут вам станеться у нагоді функція fillArray, яку ви вже написали в першому завданні та метод масиву map для того щоб змінити значення на потрібні.

*/

{
  function advancedFillArray (lenght, min, max) {
    let newArrayEmpty = new Array (lenght);
    let newArrayFull =  newArrayEmpty.fill().map (function (item) {
      return item = setRandomValue(min, max);
    })
    return newArrayFull;
  }

  function setRandomValue(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  let array  = advancedFillArray(5, 1, 15);
  console.log(array);

}