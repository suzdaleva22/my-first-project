import { Fragment, useState } from "react";
import { songs as songsData } from "./data/songs";

export default function Counter() {
  const [songsCounter, getCounter] = useState(songsData.length);
  return (
    <>
      <p className="count-title">
        Count of songs: <span className="count"> {songsCounter} </span>
      </p>
    </>
  );
}
