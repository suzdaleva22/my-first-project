export const sortParameters = [
    {
        id: 0, 
        value: 'like',
        label: "First liked songs"
    }, 
    {
        id: 1, 
        value: 'unlike',
        label: "First unliked songs"
    }
];