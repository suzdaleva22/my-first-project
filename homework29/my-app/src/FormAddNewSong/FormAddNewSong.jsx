import React from "react";
import { useEffect } from "react";
import { useState, useRef } from "react";
import { v4 as uuidv4 } from "uuid";
import './FormAddNewSong.css';

function AddSongForm({ addNewSong }) {
  const [newSong, setNewSong] = useState("");
  const inputRef = useRef("");

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const onChange = function (event) {
    setNewSong(event.target.value);
  };

  const onSubmit = function (event) {
    event.preventDefault();
    const song = {
      id: uuidv4(),
      name: newSong,
      isLiked: false,
    };
    addNewSong(song);
    setNewSong("");
  };

  return (
    <form action="" onSubmit={onSubmit}>
      <input
        type="text"
        className="input-box"
        placeholder="Song..."
        onChange={onChange}
        name="text"
        value={newSong}
        ref={inputRef}
      />
      <button className="button add" disabled={newSong == ''}>Add new Song</button>
    </form>
  );
}

export default AddSongForm;
