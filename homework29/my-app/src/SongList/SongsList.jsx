import Song from "../SongItem/Song";
import './SongList.css';

export default function SongsList({ songs, likeSong, removeSong }) {
  return (
    <div className="songs-wrapper">
      <ul className="songs">
        {songs.map((song) => (
          <Song
            song={song}
            key={song.id}
            likeSong={likeSong}
            removeSong={removeSong}
          />
        ))}
      </ul>
    </div>
  );
}
