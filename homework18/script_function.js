/*
Создайте класс Worker со следующими свойствами name, surname, rate, days. Напишите внутри класса метод getSalary(), который считает зарплату (рейт умноженный на количество отработанных дней) и метод getInfo(), который возвращает строку с информацией о полученой зарплате сотрудника

<name> <surname> got $<money>
*/

function Worker (array) {
    this.name = array.name;
    this.surname = array.surname;
    this.rate = array.rate;
    this.days = array.days;
    this.salary = this.getSalary();
  };

  Worker.prototype.getSalary = function () {
    return this.salary = this.rate * this.days;
  };
  
  Worker.prototype.getInfo = function () {
    console.log(`${this.name} ${this.surname} got ${this.salary}`);
  };
  
/*
Создайте класс Boss, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker и плюс дополнительное свойство totalProfit. Напишите метод getSalary(), который считает зарплату сотрудника так же как метод getSalary() класса Worker + 10% от прибыли (totalProfit)

Чтобы посчитать переменную totalProfit - нужно найти сумму зарплат всех сотрудников на позиции worker. Можете найти эту переменную отдельно вне классов с помощью методов массива и передавать в класс Boss уже константой.
*/

function Boss(array) {
  Worker.call(this, array);
}

Boss.prototype.getInfo = Worker.prototype.getInfo;

const totalProfit = empoyees.filter(({position}) => position === 'worker')
  .reduce(function (accumulator, currentValue) {
    return accumulator + currentValue.rate * currentValue.days;
  }, 0);

Boss.prototype.getSalary =  function () {
  return this.rate * this.days + totalProfit / 10;
}

console.log(totalProfit);

/*
Создайте класс Trainee, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker, но его метод geSalary() работает следующим образом. Во время испытательного срока (до 60 дней) сотрудник получает 70% своей зарплаты, а после испытательного срока так же как обычный сотудник.
*/

function Trainee(array) {
  Worker.call(this, array);
}
Trainee.prototype = Worker.prototype;

Trainee.prototype.getSalary = function () {
  if (this.days < 60) {
    return  this.rate * this.days * 0.7;
  } else {
    return this.rate * this.days;
  }
};

/*
Возьмите массив объектов и создайте для каждого объекта этого массива новый объект соответсвующего класса (какой класс выбрать укажет свойсвто position).
Продемонстрируйте метод getInfo() для каждого объекта в массиве.
Напишите задачу двумя способами (!): через классы и функцию-конструктор.
*/

const listOfWorkers = [];
const listOfBosses = [];
const listOfTrainees = [];
for (key of empoyees) {
  if (key.position === 'worker') {
    const worker = new Worker(key);
    worker.getInfo();
    listOfWorkers.push(worker);
  } else if (key.position === 'boss') {
    const boss = new Boss(key);
    boss.getInfo(); 
    listOfBosses.push(boss);
  } else {
    const trainee = new Trainee(key);
    trainee.getInfo();
    listOfTrainees.push(trainee);
  }
}

console.log(listOfWorkers);
console.log(listOfBosses);
console.log(listOfTrainees);
