//-----ПЕРЕМЕННЫЕ. ТИПЫ ДАННЫХ-домашка 2----
 //1) Создайте три переменные. Присвойте первой переменной числовое значение. Вторая переменная должна быть равна первой переменной, увеличенной в 3 раза. Третья переменная равна сумме двух первых. Выведите в консоль все три переменные.

    let number1 = 22;
    let number2 = number1 * 3;
    let number3 = number1 + number2;

    console.log(`Number 1: ${number1}\n Number 2: ${number2}\n Number 3: ${number3} `);

 //2) Создайте переменные х и y для хранения числа. Значения переменных задает пользователь через команду prompt(). Рассчитайте произведение, частное, разность и сумму этих значений. Результат последовательно отобразите в консоли в формате полной математической операции:


    let firstNumber = prompt('Enter the first number');
    let secondNumber = prompt('Enter the second number');
    let sum = firstNumber + secondNumber;
    let difference = firstNumber - secondNumber;
    let multiplication = firstNumber * secondNumber;
    let division = (firstNumber / secondNumber).toFixed(2);

    console.log(` ${firstNumber} +  ${secondNumber} = ${sum}`);
    console.log(` ${firstNumber} -  ${secondNumber} = ${difference}`);
    console.log(` ${firstNumber} *  ${secondNumber} = ${multiplication}`);
    console.log(` ${firstNumber} /  ${secondNumber} = ${division}`);

//3) Создайте переменную str и запишите в нее значение из prompt. Переведите ее в верхний регистр, посчитайте длину строки и выведите эти данные в консоль в форматe (это все должно быть записано в одном console.log()):

    let myStr = prompt('Write something');
    let myStrLength = myStr.length;
    let myStrBig = myStr.toUpperCase();
    let myStrSmall = myStr.toLowerCase();

    console.log(`You wrote: "${myStr}" \ it's lenght ${myStrLength}\n This is your big string: "${myStrBig}"\n \t And this is a small one: "${myStrSmall}"`);

//4)В пачке бумаги 500 листов. За неделю в офисе расходуется 1200 листов. Какое наименьшее количество пачек бумаги нужно купить в офис на 8 недель? Результат отобразите в консоли (команда console.log()).

    const sheetsInReamPaper = 500;
    const consumptionPerWeek = 1200;
    const weeksAmount = 8;
    let paperNeeds = consumptionPerWeek * weeksAmount / sheetsInReamPaper;

    console.log(`You will need packs of paper: ` + Math.ceil(paperNeeds));

//Напишите программу которая считает процент от суммы по кредиту. Процент и сумму получаем от пользователя (команда prompt()), результат выводим в консоль (команда console.log()). Учитывайте, что пользователь может отправить процент как 10 или же как 10%.

    const percent = prompt(`Enter the %`);
    const sumCredit = prompt(`Enter the Credit`);
    let result = (sumCredit / 100 * parseFloat(percent)).toFixed(2);

    console.log(parseFloat(percent) + '% of ' + sumCredit + ' will ' + result);